## E-COMMERCE API DOCUMENTATION

***TEST ACCOUNTS:***

- Regular User:
    - email: jf@mail.com
    - pwd: jf12345

- Admin User:
    - email: jestonie@mail.com
    - pwd: jestonie12
    

***ROUTES:***

- User registration (POST)
	- http://localhost:4000/users/register
    - request body: 
        - email (string)
        - password (string)

- User authentication (POST)
	- http://localhost:4000/users/login
    - request body: 
        - email (string)
        - password (string)

- Create Product (Admin only) (POST)
	- http://localhost:4000/products/create
    - request body: 
        - name (string)
        - description (string)
        - price (number)

- Retrieve all products (Admin only) (GET)
	- http://localhost:4000/products/all
    - request body: none

- Retrieve all active products (GET)
    - http://localhost:4000/products/all
    - request body: none

- Retrieve single product (GET)
    - http://localhost:4000/:productId
    - request body: none

- Update Product information (Admin only) (PUT)
    - http://localhost:4000/:productId
    - request body:
        - name (string)
        - description (string)
        - price (number)

- Archive Product (Admin only) (PUT)
    - http://localhost:4000/:productId/archive
    - request body: none

- Activate Product (Admin only) (PUT)
    - http://localhost:4000/:productId/activate
    - request body: none

- Non-admin User checkout (Create Order)
    - http://localhost:4000/order/checkout
    - request body: 
        - productId (String)
        - productName (String)
        - quantity (Number)

- Retrieve user details
    - http://localhost:4000/users/profile
    - request body: 
        - userId (String)

- Set user as admin (Admin only)
    - http://localhost:4000/users/update-as-admin
    - request body: 
        - userId (String)

- Retrieve authenticated user’s orders
    - http://localhost:4000/orders/myCart
    - request body: none

- Retrieve all orders (Admin only)
    - http://localhost:4000/orders/all
    - request body: none






