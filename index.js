
//Dependencies and Modules
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

//Allows our backend application to be available to our frontend application

const userRoutes = require("./routes/user")
const productRoutes = require("./routes/product")
const orderRoutes = require("./routes/order")

//Environment Setup
const port = 4000;

//Server Setup
const app = express();

app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.use(cors());


//Database Connection
	//Connect to our MongoDB Database
	mongoose.connect("mongodb+srv://jestonieordanez3121:6CpNC5U42mfoWYLl@batch-297.agxwbsd.mongodb.net/capstone2?retryWrites=true&w=majority",
		{
			useNewUrlParser: true,
			useUnifiedTopology: true
		}
	)
	//prompt
	let db = mongoose.connection;
	db.on('error', console.error.bind(console,'Connection error'));
	db.once('open', () => console.log('Connected to MongoDB Atlas.'));

	//[Backend Routes]
	//http://localhost:4000/users
	app.use("/users",userRoutes);
	app.use("/products",productRoutes);
	app.use("/orders", orderRoutes);

//Server Gateway Response
if(require.main === module){
	app.listen(process.env.PORT || port, ()=>{
		console.log(`API is now online on port ${process.env.PORT || port}`)
	})
}


module.exports = {app,mongoose};