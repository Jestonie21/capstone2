//Route

//Dependencies and Modules

const express = require("express");
const userController = require("../controllers/user");
const auth = require("../auth");
const { verify, verifyAdmin } = auth;

//Routing Component
const router = express.Router();

//Check email
router.post("/checkEmail",(req,res)=>{
	userController.checkEmailExists(req.body).then(resultFromController=>res.send(resultFromController))
})

//Register a user
router.post("/register",(req,res)=>{
	userController.registerUser(req.body).then(resultFromController=>res.send(resultFromController))
})

//User Authentication
router.post("/login",userController.loginUser);

//Retrieve user details
router.get("/details", verify, userController.getDetails);

//Order Product
//router.post("/orderProduct",verify,userController.orderAProducts);

// //Set User as admin
router.put("/:userId/admin", verify, verifyAdmin, userController.adminUser);

router.put('/updateprofile', verify, userController.updateProfile);










module.exports = router;
