//Route

//Dependencies and Modules

const express = require("express");
const orderController = require("../controllers/order");
const auth = require ("../auth");
const { verify, verifyAdmin } = auth;

//Routing Component
const router = express.Router();


// Non-admin user checkout route
router.post("/checkout", verify, orderController.createOrder);

//Retrieve authenticated user’s orders
router.get("/order", verify, orderController.getUserOrders);


// Retrieve All Orders (Admin Only)
router.get("/all", verify, verifyAdmin, orderController.getAllOrders);



//Routing Component
module.exports = router;