//Dependencies and Modules
const express = require("express");
const productController = require("../controllers/product");
const auth = require("../auth") 
const {verify, verifyAdmin} = auth;


//Routing Component
const router = express.Router();

//s50
//create a product
router.post("/create", verify, verifyAdmin, productController.addProduct);

//retrieving all product
router.get("/all", verify, verifyAdmin, productController.getAllProducts);

//getAllActiveProducts
router.get("/", productController.getAllActiveProducts);


//s51
//Retrieve single product
router.get("/:productId", productController.getProduct);


//Update Product information
router.put("/:productId",verify, verifyAdmin, productController.updateProduct);


//archive product
router.put("/:productId/archive",verify, verifyAdmin, productController.archiveProduct);


//activate product
router.put("/:productId/activate", verify, verifyAdmin, productController.activateProduct);


//s52



// Allows us to export the "router" object that will be accessed in our "index.js" file
module.exports = router;