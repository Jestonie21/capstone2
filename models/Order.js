const mongoose = require("mongoose");
const Product  = require("../models/Product");
const User = require("../models/User"); 

const orderSchema = new mongoose.Schema({

	userId: {
      	type: mongoose.Schema.Types.ObjectId,
        ref: "User",
      	required: [true, "User Id is required"]
    },

  orderProducts: [
    	{
    		productId: {
          	type: mongoose.Schema.Types.ObjectId,
            ref: "Product",
          	required: [true, "Product Id is required"]
        },
        quantity: {
          	type: Number,
          	required: [true, "Quantity is required"]
        }
    	}

  ],
  totalAmount: {
    type: Number
  },
  orderedOn: {
      type: Date,
      default: new Date()
  }

});


module.exports = mongoose.model('Order', orderSchema);

