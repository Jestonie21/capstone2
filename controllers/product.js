const Product = require("../models/Product");
const User = require("../models/User");
const auth = require("../auth");



//Create a new product
module.exports.addProduct = (req, res) => {

    let newProduct = new Product({
        name : req.body.name,
        description : req.body.description,
        price : req.body.price
    });

    return newProduct.save().then((product, error) => {

        if (error) {
            return res.send(false);

        } else {
            return res.send(true);
        }
    })
    .catch(err => res.send(err))
};


//Retrieve all products

module.exports.getAllProducts = (req,res)=>{
    return Product.find({}).then(result=>{
        return res.send(result)
    })
}


//getAllActiveProducts

module.exports.getAllActiveProducts = (req,res)=>{
    return Product.find({isActive:true}).then(result=>{
        return res.send(result)
    })
}


//Retrieve single product

module.exports.getProduct = (req,res)=>{
    return Product.findById(req.params.productId).then(result=>{
        return res.send(result)
    })
}


//Update Product information

module.exports.updateProduct = (req,res)=>{

    let updatedProduct = {
        name: req.body.name,
        description: req.body.description,
        price: req.body.price
    }

    return Product.findByIdAndUpdate(req.params.productId, updatedProduct).then((product, error)=>{
        if(error){
            return res.send(false);
        }else{
            return res.send(true);
        }
    })
}


//archive product

module.exports.archiveProduct = (req, res) => {

    let updateActiveField = {
        isActive: false
    }

    return Product.findByIdAndUpdate(req.params.productId, updateActiveField)
    .then((course, error) => {

        if(error){
            return res.send(false)

        } else {
            return res.send(true)
        }
    })
    .catch(err => res.send(err))

};


//activate product

module.exports.activateProduct = (req, res) => {

    let updateActiveField = {
        isActive: true
    }

    return Product.findByIdAndUpdate(req.params.productId, updateActiveField)
    .then((product, error) => {

        if(error){
            return res.send(false)

        } else {
            return res.send(true)
        }
    })
    .catch(err => res.send(err))

};