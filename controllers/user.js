//controller
const User = require("../models/User");
const Product = require("../models/Product")
const bcrypt = require("bcrypt");
const auth = require("../auth");


//Check if the email exists already
module.exports.checkEmailExists = (reqbody) =>{

	return User.find({email:reqbody.email}).then(result=>{

		//find method returns a record if a match is found
		if(result.length > 0){
			return true;
		}
		//no duplicate found
		//the user is not yet registered in the db
		else{
			return false;
		}


	})
}

//User Registration
module.exports.registerUser = (reqbody) =>{

	let newUser = new User({
		firstName: reqbody.firstName,
		lastName: reqbody.lastName,
		email:reqbody.email,
		mobileNo:reqbody.mobileNo,
		password: bcrypt.hashSync(reqbody.password,10)
	})

	return newUser.save().then((user,error)=>{

		if(error){
			return false;
		}else{
			return true
		}

	})
	.catch(err=>err)
}

//User authentication
module.exports.loginUser = (req,res)=>{

	return User.findOne({email:req.body.email}).then(result=>{
		if(result===null){
			return false;
		}else{
			//compareSync method is used to compare a non-encrypted password and from the encrypted password from the db
			const isPasswordCorrect = bcrypt.compareSync(req.body.password, result.password)
			//if pw match
			if(isPasswordCorrect){
				return res.send({access: auth.createAccessToken(result)})
			}
			//else pw does not match
			else{
				return res.send(false);
			}


		}
	})
	.catch(err=>res.send(err))
}



//Retrieve user details
module.exports.getDetails = (req, res) => {

    return User.findById(req.user.id)
    .then(result => {
        result.password = "";
        return res.send(result);
    })
    .catch(err => res.send(err))

};


//Order Product
// module.exports.orderAProducts = async (req, res) => {

// 	console.log(req.user.id);
// 	console.log(req.body.productId);

// 	if(req.user.isAdmin){
// 		return res.send("Action Forbidden");
// 	}

// 	let isUserUpdated = await User.findById(req.user.id).then(async (user) => {
// 	    const productId = req.body.productId;
	 

// 	    const product = await Product.findById(productId);
// 	    const productPrice = product.price;

// 	    if (user.orderedProducts.length > 0) {
// 	        const existingOrder = user.orderedProducts[0];
// 	        const existingProductIndex = existingOrder.products.findIndex(
// 	            (existingProduct) => existingProduct.productId === productId
// 	        );

// 	        if (existingProductIndex !== -1) {
// 	            existingOrder.products[existingProductIndex].quantity += quantity;
// 	        } else {
// 	            existingOrder.products.push({
// 	                productId: productId,
// 	                name: product.name,
// 	                price: product.price,
	                
// 	            });
// 	        }

// 	        // Calculate the totalAmount for the current order
// 	        existingOrder.totalAmount = existingOrder.products.reduce(
// 	            (total, product) => total + product.quantity * productPrice,
// 	            0
// 	        );
// 	    } else {
// 	        // If there are no existing orders, create a new one
// 	        const newOrder = {
// 	            products: [
// 	                {
// 	                    productId: productId,
// 	                    name: product.name,
// 	                    price: product.price,
// 	                    quantity: quantity,
// 	                },
// 	            ],
// 	            totalAmount: quantity * productPrice, // Initial totalAmount for the new order
// 	        };

// 	        user.orderedProducts.push(newOrder);
// 	    }

// 	    return user.save().then((user) => true).catch((err) => err.message);
// 	});

// 	if(isUserUpdated !== true){
// 		return res.send({ message: isUserUpdated});
// 	}


// 	let isProductUpdated = await Product.findById(req.body.productId).then(product => {

// 		let buyer = {
// 			userId: req.user.id
// 		}

// 		product.userOrders.push(buyer);

// 		return product.save().then(product => true).catch(err => err.message);
// 	})

// 	if(isProductUpdated !== true){
// 		return res.send({ message: isProductUpdated});
// 	}

// 	if(isUserUpdated && isProductUpdated){
// 		return res.send({message: "Order Product Successfully."})
// 	}
// };


// 
// module.exports.orderAProducts = async (req, res) => {
//   try {
//     const { productName } = req.body;

//     // Find the product by name
//     const product = await Product.findOne({ name: productName });

//     if (!product) {
//       return res.status(404).json({ message: 'Product not found' });
//     }

//     // Implement your order logic here, e.g., updating the product's order count

//     return res.status(200).json({ message: 'Product ordered successfully' });
//   } catch (error) {
//     console.error(error);
//     return res.status(500).json({ message: 'Internal server error' });
//   }
// };

//Set User as admin
module.exports.adminUser = (req, res) => {

    let updateActiveField = {
        isAdmin: true
    }

    return User.findByIdAndUpdate(req.params.userId, updateActiveField)
    .then((user, error) => {

        if(error){
            return res.send(false)

        } else {
            return res.send(true)
        }
    })
    .catch(err => res.send(err))

};


//Updateprofile
module.exports.updateProfile = async (req, res) => {
		try {

			console.log(req.user);
			console.log(req.body);
			
		// Get the user ID from the authenticated token
		  const userId = req.user.id;
	  
		  // Retrieve the updated profile information from the request body
		  const { firstName, lastName, mobileNo } = req.body;
	  
		  // Update the user's profile in the database
		  const updatedUser = await User.findByIdAndUpdate(
			userId,
			{ firstName, lastName, mobileNo },
			{ new: true }
		  );
	  
		  res.send(updatedUser);
		} catch (error) {
		  console.error(error);
		  res.status(500).send({ message: 'Failed to update profile' });
		}
	  }




