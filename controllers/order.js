const Order = require('../models/Order');
const Product = require('../models/Product');
const User = require('../models/User');
const auth = require('../auth');
const bcrypt = require("bcrypt");
const mongoose = require("mongoose");


//Non-admin user checkout
module.exports.createOrder = async (req, res) => {
  try {
    const user = req.user; // Assuming you have user data available in req.user after authentication
    
    if(user.isAdmin){
      return res.status(403).json({error: "This featrue is not available for admin users. Please use a customer account."})
    }

    let totalAmount = 0;
    let orderProducts = [];

    for(let i = 0; i < req.body.length; i++){
      const {productId} = req.body[i];

      const product = await Product.findOne({name: {$regex: new RegExp(name, 'i')}, isActive: true})

      if (!product) {
          return res.status(400).json({ error: `Product "${name}" not found or not active.` });
        }

      

      orderProducts.push({ productId: product._id});
    }

    const newOrder = new Order({
      userId: user.id,
      products: orderProducts,
      purchasedOn: new Date()
    });

    await newOrder.save();

    const orderDetails = orderProducts.map(product => ({
      productId: product.productId,
     
    }));

    res.status(201).json({message: "Order created successfully."})
  } catch(err){
    console.error(err);
    res.status(500).json({error: "An error occurred while creating the order."});
  }
};





//Retrieve Authenticated User's Orders
module.exports.getUserOrders = async (req, res) => {
  try {
    const userOrders = await Order.find({ userId: req.user.id });
    return res.send(userOrders);
  } catch (error) {
    return res.send(error);
  }
};


// Retrieve All Orders (Admin Only)
module.exports.getAllOrders = async (req, res) => {
  if (req.user.isAdmin) {
    try {
      const allOrders = await Order.find();

      return res.send(allOrders);
    } catch (error) {
      return res.send(error);
    }
  } else {
    return res.send({
      auth: "Failed",
      message: "Action Forbidden. Admin access required.",
    });
  }
};






